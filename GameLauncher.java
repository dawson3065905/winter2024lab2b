import java.util.Scanner;

public class GameLauncher{
	
	public static void main(String []args){
		Scanner inputer = new Scanner (System.in); 
		
		//blurb
		System.out.println("Welcome user!" + 
		"\r\n Press [1] for Hangman" +
		"\r\n Press [2] for Wordle");
		
		int userChoice = inputer.nextInt();
		
		//helper function to keep things clean
		//(otherwise, I will need add whole bunch of code
		//in main which sucks)
		if(userChoice == 1)
			launchHangman();
		
		else if(userChoice == 2)
			launchWordle();
		//failsafe
		else
			System.out.println("Invalid input");
		
		//closes scanner
		inputer.close(); 
	}
	
	public static void launchHangman(){
		//same code from hangman's main
		Scanner inputer1 = new Scanner (System.in);

		System.out.println("Enter a four-lettered word with no repeating letter: ");
		String word = inputer1.nextLine();
		Hangman.runGame(word.toUpperCase());
		//closes scanner
		inputer1.close();
	}
	
	public static void launchWordle(){
		//unnecessary, mainly for symmetry with Hangman purposes
		Scanner inputer2 = new Scanner (System.in);
		boolean	 continueLoop = true;

		while(continueLoop){
			System.out.println("Welcome to" + 
			"\u001B[32m" + " Wordle!" + "\u001B[0m"+ 
			"\r\n Press [1] if you need instructions" +
			"\r\n press [2] if you do not");
			
			int wordleChoice = inputer2.nextInt();
			
			
			if(wordleChoice == 1){
				//code from the wordle's main, just explains game
				System.out.println("The goal is to guess a 5-letter word within 6 attempts.");
				System.out.println("There will be clues about which letters in the word they guessed were correct");
				System.out.println("\u001B[32m" + "Green " + "\u001B[0m" + "= correct letter and correct position");
				System.out.println("\u001B[33m" + "Yellow " + "\u001B[0m" + "= correct letter but wrong position");
				System.out.println("White = letter is not in the word");
				
				//runs game after blurb
				//gotta remember that any functions from Wordle
				//needs to be proceed with "Wordle."
				Wordle.runGame(Wordle.generateWord());
			}
			
			else if(wordleChoice == 2)
				//runs game
				Wordle.runGame(Wordle.generateWord());
			
			else
				//failsafe
				System.out.println("Invalid input");
			
			System.out.println("Play again? 1 for yes, 2 for no");
			
			int loopChoice = inputer2.nextInt();
			 
			if(loopChoice == 1)
				continueLoop = true;
			
			else 
				continueLoop = false;
		}
		
		//closes scanner
		inputer2.close();
	}
}
