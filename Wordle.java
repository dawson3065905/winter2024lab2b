import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;

public class Wordle{
	/*public static void main(String[] args){
		System.out.println("Welcome to" + "\u001B[32m" + " Wordle!" + "\u001B[0m");
		System.out.println("The goal is to guess a 5-letter word within 6 attempts.");
		System.out.println("There will be clues about which letters in the word they guessed were correct");
		System.out.println("\u001B[32m" + "Green " + "\u001B[0m" + "= correct letter and correct position");
		System.out.println("\u001B[33m" + "Yellow " + "\u001B[0m" + "= correct letter but wrong position");
		System.out.println("White = letter is not in the word");
		runGame(generateWord());
		
		//DEBUG GOES HERE 
		
		String test = generateWord();
		System.out.println(test);
		System.out.println(isLetterInWord(test, 'z'));
		System.out.println(isLetterInSlot("Coped", 'e', 3));
		System.out.println(presentResults("Ember", guessWord("BLAME", "EMBER")));
		
	}*/
	
	
	public static String generateWord(){
		Random chooser = new Random();
		
		//choose between 0 and 19, since index is also between 0 and 19
		int randIndex = chooser.nextInt(19);
		
		String[] wordList = new String[]{
			"AMBER", "BLAME", "CLIMB", "DANCE", "BRAVE",
			"FLUTE", "GRAPE", "HASTE", "IVORY", "JUMBO",
			"JOUST", "LATCH", "MIRTH", "NUDGE", "OLIVE",
			"PLAID", "QUIRK", "VOWEL", "SNARE", "YACHT"};
			
		//uppercase for just in case
		/*"AMBER", "BLAME", "CLIMB", "DANCE", "BRAVE",
		"FLUTE", "GRAPE", "HASTE", "IVORY", "JUMBO",
		"JOUST", "LATCH", "MIRTH", "NUDGE", "OLIVE",
		"PLAID", "QUIRK", "VOWEL", "SNARE", "YACHT"
		
		"Amber", "Blame", "Climb", "Dance", "Brave", 
		"Flute", "Grape", "Haste", "Ivory", "Jumbo",
		"Joust", "Latch", "Mirth", "Nudge", "Olive",
		"Plaid", "Quirk", "Vowel", "Snare", "Yacht"*/
		
		return wordList[randIndex];
	}
	
	public static boolean isLetterInWord(String word, char c){
		//recycled code from hangman
		boolean isletter = false;
		
		
		//similar to printWork, takes advantage of i incrementing
		//i is basically synchronized to index of the word
		for(int i = 0; i < word.length(); i++){
			if(c == word.charAt(i))
				isletter = true;
		}
		
		//returns the index letter
		return isletter;
	}
	
	public static boolean isLetterInSlot(String word, char letter, int position){
		
		//runs word.length first to avoid out of bounds error
		if(word.length() > position-1 && word.charAt(position) == letter)
			return true;
		else
			return false;
	}
	
	public static String[] guessWord(String answer, String guess){
		String[] coloured = new String[answer.length()];
		
		for(int i = 0; i < guess.length(); i++){
			char charOfChoice = guess.charAt(i);
			//i also represents the current index of answer
			
			//is the letter at least present in the guess
			if(isLetterInWord(answer, charOfChoice)){
				//is the letter in the right slot
				//if yes, then green
				if(isLetterInSlot(answer, charOfChoice, i)){
					coloured[i] = "green";
				}
				//else, then yellow
				else{
					coloured[i] = "yellow";
				}
			}
			
			//if it's not, then white
			else{
				coloured[i] = "white";
			}
		}
		
		return coloured;
	}
	
	public static String presentResults(String word, String[] colours){
		String displayWord = "";
		
		//uses same tech as printWork from hangman
		//builds up a string over time, 
		//using if statments for different colors
		//reads from previously created array in guessWord
		for(int i = 0; i < colours.length; i++){
			//reads element of array based on i
			//if the string is "green"
			//append ANSI code at beginning
			//add character of the input word at index to string we are building
			if(colours[i].equals("green"))
				displayWord += "\u001B[32m" + word.charAt(i) + "\u001B[0m" + " ";
			if(colours[i].equals("yellow"))
				displayWord += "\u001B[33m" + word.charAt(i) + "\u001B[0m" + " ";
			if(colours[i].equals("white"))
				displayWord += "\u001B[0m" + word.charAt(i) + "\u001B[0m" + " ";
		}
		
		return displayWord;
	}
	
	public static String readGuess(){
		Scanner inputer = new Scanner (System.in);
		
		boolean keepLoop = true;
		String inputWord = "";
		
		while(keepLoop){
			//takes in user input
			System.out.println("Input a five letter word: ");
			//sets previously declared string to input
			inputWord = inputer.nextLine();
			
			//if the word is 5 character
			//break loop using boolean
			if(inputWord.length() == 5){
				keepLoop = false;
			}
			//else, keep running loop + println
			else{
				System.out.println("Not 5 letter word, try again");
			}
				
		}
		
		//returns user input in uppercase to avoid case issues
		return inputWord.toUpperCase();
		
	}
	
	public static void runGame(String answer){
		int guessAmount = 6;
		boolean runLoop = true;
		
			while(runLoop && guessAmount > 0){
				//verifies input using readGuess
				String userGuess = readGuess();
				//gets "dye sequence" for presentResults
				String[] userArray = guessWord(answer, userGuess);
				
				//prints out statues of user's guess using the random answer 
				//and the previously declared variable and userArray  
				System.out.println(presentResults(userGuess, userArray));
				
				//breaks loop based on conditions
				if(userGuess.equals(answer)){
					runLoop=false;
				}
				else{
					guessAmount--;
					System.out.println(guessAmount + " guesses left");
				}
			}
			
			//if the player break the loop by not guessing the word
			//correctly, the boolean true and we will know that they lost the match
			if(runLoop == true)
				System.out.println("Game Over! The word was: " + "\u001B[32m" + answer + "\u001B[0m");	
			else
				System.out.println("Congration! You done it!");
	}
		
}